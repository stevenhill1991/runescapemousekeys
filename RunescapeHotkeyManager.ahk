#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
;#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%

gui := new Gui

Class Gui {

	__New(){
		;creates a settings helper object, creates the window, creates the components, and creates a variable to store the current selected script
		this.settingsHelper := new SettingsHelper
		this.initGui()
		this.initComponents()
		this.initScriptSelectorList()
		this.selectedScriptName := ""
	}

	initGui(){
		;creates the window to hold the components
		Gui, Show, x127 y87 h379 w479, Hotkey manager
		Return
	}

	initComponents(){
		;adds the components to the window and binds the methods to the buttons
		global


		scriptListBox := this.scriptListBox.bind(this)
		Gui, Add, ListBox, vScriptListBox x12 y9 w130 h320
		GuiControl +g, ScriptListBox, % scriptListBox


		newScript := this.newScript.bind(this)
		Gui, Add, Button,vNewScript x12 y339 w130 h30 , New Script
		GuiControl +g, NewScript, % newScript



		Gui, Add, Tab, vScriptTabs x152 y9 w190 h0, Welcome|Hotkeys


		Gui, Tab, Hotkeys

		hotKeysListView := this.hotKeysListView.bind(this)
		Gui, Add, ListView,vHotKeyList x152 y29 w180 h295, Hotkey|Method|Param
		GuiControl +g, HotKeyList, % hotKeysListView

		Gui, Add, Text, x152 y9 w180 h20 , Hotkeys
		Gui, Add, Hotkey,vSuspendHotkey x422 y79 w40 h30
		Gui, Add, Text, x362 y79 w60 h30 , Suspend Hotkey
		Gui, Add, Edit,vActiveWindow x362 y139 w100 h30
		Gui, Add, Text, x362 y119 w100 h20 , Activate on window


		Gui, Add, Button, x362 y9 w100 h30 , Run

		deleteScript := this.deleteScript.bind(this)
		Gui, Add, Button,vDeleteScript x362 y299 w100 h30 , Delete Script
		GuiControl +g, DeleteScript, % deleteScript

		Gui, Add, GroupBox, x352 y49 w120 h130 , Settings
		Gui, Add, Button, x362 y249 w100 h30 , Clear Keys


		Return
	}

	initScriptSelectorList(){
		;initialises the list of scripts using the settings.ini sections [scriptName]
		scriptList := this.settingsHelper.getSections()
		if(scriptList){
			this.addListElements(scriptList)
		}
		return
	}

	addListElements(scriptList){
		;adds each settings.ini section [scriptName] to the list component
		Loop,parse,scriptList,`n
		{
			GuiControl, ,ScriptListBox, %A_LoopField%
		}
	}

	newScript(){
		GuiControl, Choose, ScriptListBox, 0
		GuiControl, Choose, ScriptTabs, 2
		return
	}

	deleteScript(){
		; asks for confirmation, removes the section [scriptName] from the settings.ini file and redraws the ui
		if(this.selectedScriptName){
			MsgBox, 1, Delete script?, Confirm deletion of this script
			IfMsgBox Ok
			{
				this.settingsHelper.removeSection(this.selectedScriptName)
				this.resetGui()
				return
			}
			Else
			{
				return
			}
		}
		return
	}

	resetGui(){
		; resets the selected script name, clears the script selector list and reinitialises the list, switches ui state to no script selected
	  this.selectedScriptName := ""
		GuiControl, , ScriptListBox, |
		this.initScriptSelectorList()
		GuiControl, Choose, ScriptTabs, 1
		return
	}

	scriptListBox(){
		; gui control to populate fields when a script is selected from the list box
		if A_GuiEvent <> LeftClick
				GuiControlGet, selectedScript,,ScriptListBox
				if(!selectedScript){
					return
				}
				this.populateFields(selectedScript)
				GuiControl, Choose, ScriptTabs, 2
		return
	}

	hotKeysListView(){
		; if an existing row in the hotkeys list is double clicked, edit the current hotKeysListView
		; if row 0 is double clicked, add a new hotkey
		MsgBox firing
		if A_GuiEvent = DoubleClick
   			ToolTip You double-clicked row number %A_EventInfo%.
   			if(A_EventInfo > 0){
   				this.showHotKeyEditor(A_EventInfo)
   			}
		return
	}

	showHotKeyEditor(rowNumber){

	}

	initActions(){
		; tell the close button (X) to close the script when pressed
		GuiClose:
		ExitApp
	}

	populateFields(selectedScript){
		; get keys for section in settings.ini file [scriptName] and populate the hotkeys listview for the selected script
		this.selectedScriptName := selectedScript
		MsgBox populating fields for %selectedScript%
		scriptData := this.settingsHelper.getKeys(selectedScript)
		this.populateListView(scriptData)
		return
	}

	populateListView(hotkeys){
		; clear the hotkeys list view
		; if no hotkeys exist return
		; if hotkeys exist add them to the hotkeys list view
		LV_Delete()
		if(!hotkeys){
			MsgBox saved script has no hotkeys return
			return
		}
		MsgBox populating hotkeys %hotkeys%
		Loop,parse,hotkeys,`n
		{
			StringSplit, keySplit,A_LoopField,=
			key = % keySplit1
			StringSplit, methodSplit, keySplit2,`,
			method = % methodSplit1
			if(methodSplit0 > 2){
				param = [%methodSplit2%,%methodSplit3%]
			} else {
				param = % methodSplit2
			}
			LV_Add("",key,method,param)
		}
		return
	}
}

Class SettingsHelper {
  ; class used to provide quick access to the settings.ini file


	fileName := "settings.ini"

	__New(){

	}

	saveSection(pairs, section){
		IniWrite, %pairs%, % this.fileName, %section%
	}

	removeSection(section){
		IniDelete, % this.fileName, %section%
	}

	removeSections(){
		FileDelete, % this.filename
	}

	getSections(){
		IniRead, sections, % this.fileName
		return sections
	}

	getKeys(section){
		IniRead, keys, % this.fileName, %section%
		return keys
	}

	enableHotkeys(hotkeys){
		for each, key in hotkeys {
			Hotkey, %key%, On
		}
		return true
	}

	disableHotkeys(hotkeys){
		for each, key in hotkeys {
			Hotkey, %key%, Off
		}
		return true
	}

}
